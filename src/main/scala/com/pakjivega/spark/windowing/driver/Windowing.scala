package com.pakjivega.spark.windowing.driver

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}

object Windowing {
  def main(args: Array[String]): Unit = {
    println("Entro en Spark")
    val folderTable: String = args(0)
//    val folderTable: String = "/home/loekas/jorge/repos/sparkwindowing/src/test/resources/data/flight/flights.csv"
    println("folderTable is: " + folderTable);
    val islocal: Boolean = false


    val sc:SparkContext = if (islocal) {
      println("Antes de")
      val conf = new SparkConf()
        .setMaster("local[1]")
      .setAppName("Nada")
      .set("spark.cores.max", "10")
      println("Despues de")

      new SparkContext(conf)


//      val conf = new SparkConf()
//      println("Despues de")
//      conf.set("spark.broadcast.compress", "false")
//      conf.set("spark.shuffle.compress", "false")
//      conf.set("spark.shuffle.spill.compress", "false")
//      new SparkContext("local[2]", "FlightCalculation", conf)
    } else {
      val conf = new SparkConf().setAppName("FlightCalculation")
      new SparkContext(conf)
    }
    println("Ya tenemos sc")
    val sqlContext = new SQLContext(sc)

    val df: DataFrame = sqlContext.read.parquet(folderTable)
    println("Number of rows is: " + df.count())
  }

}
