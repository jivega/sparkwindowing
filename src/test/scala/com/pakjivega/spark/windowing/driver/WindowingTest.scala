package com.pakjivega.spark.windowing.driver

//import org.scalatest.FlatSpec
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

import scala.collection.mutable


class WindowingTestcextends extends FunSuite with
  BeforeAndAfterEach with BeforeAndAfterAll {

  @transient var sc: SparkContext = null
  val folderTable: String = "src/test/resources/data/flight/flights.csv"

  override def beforeAll(): Unit = {

    val envMap = Map[String, String](("Xmx", "512m"))
    val sparkConfig = new SparkConf()
    sparkConfig.set("spark.broadcast.compress", "false")
    sparkConfig.set("spark.shuffle.compress", "false")
    sparkConfig.set("spark.shuffle.spill.compress", "false")
    sparkConfig.set("spark.io.compression.codec", "lzf")
    sc = new SparkContext("local[2]", "WindowingUnitTest", sparkConfig)
  }

  override def afterAll(): Unit = {
    sc.stop()
  }

  test("Test word count") {
    val myDF = sc.textFile(folderTable)
    assert(myDF.count() == 40, "There are not 40 items in the file: " + myDF.count())

  }
}
