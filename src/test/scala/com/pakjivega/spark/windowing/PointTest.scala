package com.pakjivega.spark.windowing

import org.scalatest.FlatSpec

class PointTest extends FlatSpec{
  "A Point" should "have a fix x  value and y value" in {
    val point:Point = new Point(3,6)

    assert( point.x === 3)
    assert( point.y === 6)
  }
}
